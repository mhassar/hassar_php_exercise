# PHP Exercise

Distribute a total amount randomly (within certain parameters) in a range of dates, excluding weekends. There should be a baseline that will define the minimum amount of the value assigned to a specific date. The returned result should be a unidimensional array.

# Exercise live demo

You can see a online demo of my exercise ( i used the zeit platforme ) https://phpinterview.hassarov099.now.sh

# Evaluation criteria

1- I use a friendly user interface to display the results using HTML, CSS, jQuery and Bootstrap.  
2- Asyncronous requests (using AJAX and JSON) from index.html to exercise.php  
3- Input parsing to prevent code injection/execution using Javascript input controls and PHP Sanitize.  