let has_error = 0
$(document).ready(function() {
    let inputs = [
        'total','baseline','from','to'
    ]
    $('#submit').click(function(e) {
        e.preventDefault()
        has_error = 0
        for (let i = 0; i < inputs.length; i++) {
            has_error = validate(inputs[i])
        }
        if(has_error) return false
        $('.loader').removeClass('d-none');
        $(this).attr('disabled','true');
        
        $.ajax({
            url: "exercise.php",
            type: "POST",
            dataType: 'JSON',
            data: {
                total: $('#total').val(),
                baseline: $('#baseline').val(),
                from: $('#from').val(),
                to: $('#to').val()
            },
            success: function(data){
                let total = 0
                let last = 0
                let count = Object.keys(data.message.data).length;
                $('#details').removeClass('d-none')
                $('#res_baseline').text(data.message.inputs.baseline)
                $('#res_total').text(data.message.inputs.total)
                $('#res_dates').text('From ' + data.message.inputs.from + ' To '+ data.message.inputs.to)
                $('#res_min_amount').text(data.message.min_amount)
                $('#res_workdays').empty()
                $('#after_total').empty()
                
                $.each(data.message.data, function(key, value){
                    last++
                    $('#res_workdays').append(`<code>${key} => ${value}</code><br>`)
                    $('#after_total').append(` ${value}`)
                    console.log(last,count);
                    
                    if(last < count) $('#after_total').append(` +`)
                    total += parseFloat(value)
                })
                $('#after_total').append(` = ${parseInt(total)}`)
            },
            error: function(error) {
                $("html, body").animate({ scrollTop: 0 }, "slow")
                console.error(error.responseJSON)
                alert('Ooops! '+ error.responseJSON.message)
            }
        })
        .always(function() {
            $('.loader').addClass('d-none');
            $('#submit').removeAttr('disabled');
        });
    })
})
function validate(element) {
    if(!$(`#${element}`).val()) {
        $(`#${element}_err`).removeClass('d-none')
        has_error = 1
    } else {
        $(`#${element}_err`).addClass('d-none')
    }

    if(element == 'baseline' && (!$(`#${element}`).val() || $(`#${element}`).val() > 100 || $(`#${element}`).val() < 0)) {
        $(`#${element}_err`).removeClass('d-none')
        has_error = 1
    }
    return has_error
}