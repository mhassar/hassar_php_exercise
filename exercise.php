<?php
	class Plogg {
		/* JSON Response */
		public function JsonResponse($code = 200,$message = null){
			header_remove();
			http_response_code($code);
			header("Cache-Control: no-transform,public,max-age=300,s-maxage=900");
			header('Content-Type: application/json');
			$status = array(
				200 => '200 OK',
				400 => '400 Bad Request',
				422 => 'Unprocessable Entity',
				500 => '500 Internal Server Error'
			);
			header('Status: '.$status[$code]);
			// return the encoded json
			return json_encode(array(
				'status' => $code < 300, // success or not?
				'message' => $message
			));
		}
	
		/* Get the Working days number*/
		public function WorkingDays($from, $to){
			$workingDays = [1, 2, 3, 4, 5];
			$holidayDays = ['*-12-25', '*-01-01', '2013-12-23'];
	
			$from = new DateTime($from);
			$to = new DateTime($to);
			$to->modify('+1 day');
			$interval = new DateInterval('P1D');
			$periods = new DatePeriod($from, $interval, $to);
	
			$days = 0;
			foreach ($periods as $period) {
				if (!in_array($period->format('N'), $workingDays)) continue;
				if (in_array($period->format('Y-m-d'), $holidayDays)) continue;
				if (in_array($period->format('*-m-d'), $holidayDays)) continue;
				$days++;
			}
			return $days;
		}

		public function Sanitize($value,$type){
			return filter_var($value, $type == 'str' ? FILTER_SANITIZE_STRING : FILTER_SANITIZE_NUMBER_INT);
		}
	
		/* Get a normalized array of random based on working days */
		private function RandArray($week_days){
			$total = 0;
			$rand_number = 0;
			$weekdays = [];
			$normalized_rand = array();		
			for ($i= 0 ; $i < $week_days ; $i++) {
				$rand_number = mt_rand(1,100000); // We need higher precision if the interval of weekdays is long
				$total += $rand_number;
				$weekdays[] = $rand_number;
			}
			$total = (float)$total;
			foreach ( $weekdays as $wd ) {	
				$normalized_rand[] = ((float)$wd / $total);		
			}
			return $normalized_rand;
		}
	
		/* Main function that take client params and do a random distribution */
		public function AmountRandDist($amount, $baseline, $from, $to){
			$start_day = strtotime($from);
			$end_day = strtotime($to);
	
			$num_weekdays = $this->WorkingDays($from, $to);
			$norm_array = $this->RandArray($num_weekdays);
	
			$normalized_index = 0;
			$output_array = [];
	
			$min_days = (float)$amount * (float)$baseline / ((float)$num_weekdays*100.0);
			$amount_to_be_dist = (float)(100-$baseline) * (float)$amount / 100.0;
			//fill the output array
			while ($start_day <= $end_day ) {		
				if (date("N", $start_day) < 6){
					//Non-zero array element
					$amount_for_weekday = $norm_array[$normalized_index] * $amount_to_be_dist + $min_days;
					//Two decimal number
					$formatted_amount = number_format($amount_for_weekday, 2,'.','');
					$normalized_index++;
				} else {
					//Zero array element
					$formatted_amount = "0.00";	
				}
				$current_date = date('Y-m-d',$start_day);
				$output_array += [$current_date => $formatted_amount];	
				$start_day += 86400; //86400 = one day of seconds
			}
			return $output_array;
		}
	}
	
	$plogg = new Plogg();
	
	$_POST["total"] = $plogg->Sanitize($_POST["total"],'int');
	$_POST["baseline"] = $plogg->Sanitize($_POST["baseline"],'int'); 
	$_POST["from"] = $plogg->Sanitize($_POST["from"],'str');
	$_POST["to"] = $plogg->Sanitize($_POST["to"],'str');

	if (!$_POST["total"] || !$_POST["baseline"] || !$_POST["from"] || !$_POST["to"]) 
					die($plogg->JsonResponse(422,'Unprocessable Entity'));
	
	try {
		$result  = $plogg->AmountRandDist(
			$_POST["total"],
			$_POST["baseline"],
			$_POST["from"],
			$_POST["to"]
		);
		$week_days = $plogg->WorkingDays( $_POST["from"], $_POST["to"]);

		$min_amount = number_format(
			((float)$_POST["total"] * (float) $_POST["baseline"]) / (float)((int)$week_days*100), 2,'.',''
		);
		
		die($plogg->JsonResponse(200, array(
			'data' => $result,
			'min_amount' => $min_amount,
			'inputs' => $_POST
		)));
	} catch (\Throwable $th) {
		die($plogg->JsonResponse(500, $th->getMessage()));
	}
?>




